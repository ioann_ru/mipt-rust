# Additional reading list

This document contains additional reading links and videos. It's not necessary to read or watch them, but it may help you understand Rust deeply.

## "Rustbooks"

- [The Rust Programming Language](https://doc.rust-lang.org/book/) - Rustbook, the first book to be read by newbee.
- [The Rust Reference](https://doc.rust-lang.org/stable/reference/) - The Rust language reference. Something like explained docs.
- [Learn Rust With Entirely Too Many Linked Lists](https://rust-unofficial.github.io/too-many-lists/) - Just writing multiple linked lists. May help to understand the basics of ownership.
- [The Cargo Book](https://doc.rust-lang.org/cargo/) - The Cargo package manager reference.
- [Rust Cookbook](https://rust-lang-nursery.github.io/rust-cookbook/intro.html) - Explains how to solve simple, widespread programming tasks.
- [The Little Book of Rust Macros](https://danielkeep.github.io/tlborm/book/index.html) - Starter book about Rust Macros.
- [Rust Design Patterns](https://rust-unofficial.github.io/patterns/intro.html) - Good practices of Rust programming.
- [The Rustonomicon](https://doc.rust-lang.org/nomicon/index.html) - The book about Unsafe Rust. In addition, covers some advanced safe topics such as ownership.
- [Rust's Unsafe Code Guidelines Reference (UCG WG)](https://rust-lang.github.io/unsafe-code-guidelines/introduction.html)
- [The Rust Performance Book](https://nnethercote.github.io/perf-book/title-page.html) - The book about optimizing Rust code.
- [Asynchronous Programming in Rust](https://rust-lang.github.io/async-book/01_getting_started/01_chapter.html) - Introduction to asynchronous programming.
- [Futures Explained in 200 Lines of Rust](https://cfsamson.github.io/books-futures-explained/introduction.html) - Good book on futures and executors.
- [Guide to Rustc Development](https://rustc-dev-guide.rust-lang.org) - The guide to compiler development for entusiasts.

## Articles

- [Improved portability and performance](https://pngquant.org/rust.html) - How [libimagequant](https://github.com/ImageOptim/libimagequant) library became more performant and **portable (!)** by rewriting C code to Rust.
- [How to organize your Rust tests](https://blog.logrocket.com/how-to-organize-your-rust-tests/) - Answers the question "How to organize your Rust tests" :)

## Writing idiomatic Rust code

- [Writing Idiomatic Libraries in Rust](https://www.youtube.com/watch?v=0zOg8_B71gE) - Good talk on writing idiomatic code.

## Conference talks about Rust

- [The History of Rust](https://www.youtube.com/watch?v=79PSagCD_AY) - Just a bit of History of Rust.
- [How Rust Views Tradeoffs](https://www.youtube.com/watch?v=2ajos-0OWts) - Good talk about aims of Rust language.
- [RustConf 2021 - Move Constructors: Is it Possible? by Miguel Young de la Sota](https://www.youtube.com/watch?v=UrDhMWISR3w)
- [Rust's Journey to Async/Await](https://www.youtube.com/watch?v=lJ3NC-R3gSI) - The talk about high-level design of async in Rust.
- [The Talk You've Been Await-ing for](https://www.youtube.com/watch?v=NNwK5ZPAJCk) - Introduction to async in Rust.
- [Rust Programming Techniques](https://www.youtube.com/watch?v=vqavdUGKeb4) - Good practices of writing idiomatic code.

## YouTube channels

- [Jon Gjengset YouTube channel](https://www.youtube.com/c/JonGjengset/featured) - Excellent channel about Rust, especially "Crust of Rust" series, where several topics covered from intermediate to advanced level.
- [Aleksey Kladov YouTube channel](https://www.youtube.com/channel/UCLd3PQ6J0C-VuNBozsXGUWg/featured) - If you're interested on how `rust-analyzer` works - it's the best channel and the best speaker.

## Research papers

- [GhostCell: Separating Permissions from Data in Rust](http://plv.mpi-sws.org/rustbelt/ghostcell/paper.pdf) - Tricky usage of Rust type system.
- [Stacked Borrows: An Aliasing Model for Rust](https://plv.mpi-sws.org/rustbelt/stacked-borrows/paper.pdf) - On checking `unsafe` code.

## Books

- ["Rust for Rustaceans - Idiomatic Programming for Experienced Developers" by Jon Gjengset](https://nostarch.com/rust-rustaceans) - Very good book that covers **a lot** of details about the language in-depth.

## Blogs

- [Alastair Reid blog](https://alastairreid.github.io) - In this blog you can find great posts about automatic verification tools.
- [Aleksey Kladov blog](https://matklad.github.io) - Some random things about Rust.
- [Waffle blog](https://ihatereality.space) - Some random things about Rust.
- [Lloyd Chan blog](https://beachape.com) - The author usually writes on advanced usages of type system.
